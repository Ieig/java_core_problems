# Topics of the Course

### 1. Git and Java Basics
### 2. Language Constructs. Primitive Data Types
### 3. Reference Data Types
### 4. OOP. Encapsulation
### 5. OOP. Inheritance
### 6. OOP. Polymorphism
### 7. Generics
### 8. Exceptions
### 9. I/O
### 10. Complexity. Data Structures. Collections Framework
### 11. Algorithms and Its Complexity
### 12. Annotations and Reflection API
### 13. Multithreading 1
### 14. Multithreading 2
### 15. Java 8 New Features
