package s02;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        int n = requestNumber();
        int array[] = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = requestNumber();
        }
        int sumOfEven = 0;
        int prodOfOdd = 0;
        if (n > 1) {
            prodOfOdd = 1;
        }

        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                sumOfEven += array[i];
            }
            else {
                prodOfOdd *= array[i];
            }
        }

        System.out.println(sumOfEven);
        System.out.println(prodOfOdd);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
