package s02;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        int DAYS = 30;
        int[] array = new int[DAYS];
        for (int i = 0; i < DAYS; i++) {
            array[i] = (int) (Math.random() * 100);
        }

        for (int i : array) {
            System.out.println(i);
        }

        System.out.println('\n');

        int minVal = array[0];
        int minCount = 1;

        int maxVal = array[0];
        int maxCount = 1;

        for (int i = 1; i < DAYS; i++) {
            if  (minVal == array[i]) {
                minCount += 1;
            }
            if (minVal > array[i]) {
                minVal = array[i];
                minCount = 1;
            }

            if (maxVal == array[i]) {
                maxCount += 1;
            }
            if (maxVal < array[i]) {
                maxVal = array[i];
                maxCount = 1;
            }
        }

        int [] minPosArray = new int[minCount];
        int [] maxPosArray = new int[maxCount];

        int j = 0;
        int k = 0;

        for (int i = 0; i < DAYS; i++) {
            if (array[i] == minVal) {
                minPosArray[j++] = i;
            }
            if (array[i] == maxVal) {
                maxPosArray[k++] = i;
            }
        }

        System.out.println(minVal);

        for (int i : minPosArray) {
            System.out.println(i);
        }

        System.out.println(maxVal);

        for (int i : maxPosArray) {
            System.out.println(i);
        }
    }
}
