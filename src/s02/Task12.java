package s02;

import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        int travelDist = requestNumber();
        int fuelCons = requestNumber();
        int literPrice = requestNumber();

        double fuelUsed = travelDist / 100.0 * fuelCons;
        double moneySpent = fuelUsed * literPrice;

        System.out.println(fuelUsed);
        System.out.println(moneySpent);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
