package s02;

public class Task13 {
    public static void main(String[] args) {
        int N = 1000;
        double[] array = new double[N];

        for (int i = 0; i < N; i++) {
            array[i] = Math.random();
        }

        double arraySum = 0;

        for (double i : array) {
            arraySum += i;
        }
        double arMean = arraySum / N;

        System.out.println(arMean);
    }
}
