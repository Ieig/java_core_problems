package s02;

import java.util.Scanner;

public class Task14 {
    public static void main(String[] args) {
        int n = requestNumber();

        int sum = 0;
        int powered = 1;
        for (int i = 0; i <= n; i++) {
            sum += powered;
            powered *= 2;
        }

        System.out.println(sum);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
