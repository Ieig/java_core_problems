package s02;

import java.util.Scanner;

public class Task15 {
    public static void main(String[] args) {
        int x = requestNumber();
        int y = requestNumber();

        int res = 0;
        for (int i = 0; i < y; i++) {
            res += x;
        }

        System.out.println(res);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
