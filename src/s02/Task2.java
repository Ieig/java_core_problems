package s02;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        int n = requestNumber();

        int hoursThisDay = n / 3600;
        int minutesThisHour = (n % 3600) / 60;
        int secondsThisMinute = n % 60;

        System.out.println(hoursThisDay);
        System.out.println(minutesThisHour);
        System.out.println(secondsThisMinute);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
