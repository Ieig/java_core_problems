package s02;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        int n = requestNumber();

        int tens = n / 10;
        int remainder = n % 10;
        int digitSum = tens + remainder;
        int digitProd = tens * remainder;

        System.out.println(tens);
        System.out.println(remainder);
        System.out.println(digitSum);
        System.out.println(digitProd);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
