package s02;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        int x = requestNumber();
        int y = requestNumber();
        int z = requestNumber();

        boolean isPifTriple = false;
        if (x != 0 && y != 0 && z != 0 && x != y && x != z && y != z) {
            if (x * x == y * y + z * z || y * y == x * x + z * z || z * z == x * x + y * y) {
                isPifTriple = true;
            }
        }

        System.out.println(isPifTriple);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
