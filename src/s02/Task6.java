package s02;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        int radius = requestNumber();
        double length = 2 * Math.PI * radius;
        double square = Math.PI * Math.pow(radius, 2);

        System.out.println(length);
        System.out.println(square);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
