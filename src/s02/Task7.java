package s02;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        int x = requestNumber();
        double y;
        if (x > 0) {
            y = Math.pow(Math.sin(x), 2);
        }
        else {
            y = 1 - 2 * Math.sin(x * x);
        }
        System.out.println(y);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
