package s02;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        int n = requestNumber();
        for (int i = 1; i < 11; i++) {
            System.out.println(i * n);
        }
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
