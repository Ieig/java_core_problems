package s02;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        int n = requestNumber();
        int digitSum = 0;
        while (n > 0) {
            digitSum += n % 10;
            n /= 10;
        }
        System.out.println(digitSum);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
