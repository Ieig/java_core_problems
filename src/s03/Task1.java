package s03;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        System.out.println("Введите название государства:");
        String country = requestString();
        System.out.println("Введите название столицы:");
        String capital = requestString();

        System.out.println("Столица государства " + country + " — город " + capital);
    }
    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
