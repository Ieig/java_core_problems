package s03;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        String str = requestString();
        boolean isWrong = false;

        for (int i = 0; i < str.length()-1; i++) {
            if (str.charAt(i) == 'Ж' || str.charAt(i) == 'Ш' || str.charAt(i) == 'ж' || str.charAt(i) == 'ш') {
                if (str.charAt(i+1) == 'ы') {
                    isWrong = true;
                    break;
                }
            }
        }
        if (isWrong) {
            System.out.println("Есть ошибка!");
        }
        else {
            System.out.println("Ошибок нет.");
        }
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        return scanner.nextLine();
    }
}
