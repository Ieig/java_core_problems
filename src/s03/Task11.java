package s03;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        String strRus = requestString();
        StringBuilder strTran = new StringBuilder();

        for (int i = 0; i < strRus.length(); i++) {
            switch(strRus.charAt(i)) {
                case 'а' :
                case 'э' :
                    strTran.append('a');
                    break;
                case 'б' :
                    strTran.append('b');
                    break;
                case 'в' :
                    strTran.append('v');
                    break;
                case 'г' :
                    strTran.append('g');
                    break;
                case 'д' :
                    strTran.append('d');
                    break;
                case 'е' :
                case 'ё' :
                    strTran.append('e');
                    break;
                case 'ж' :
                    strTran.append('z');
                    strTran.append('g');
                    break;
                case 'з' :
                    strTran.append('z');
                    break;
                case 'и' :
                case 'й' :
                case 'ы' :
                    strTran.append('i');
                    break;
                case 'к' :
                    strTran.append('k');
                    break;
                case 'л' :
                    strTran.append('l');
                    break;
                case 'м' :
                    strTran.append('m');
                    break;
                case 'н' :
                    strTran.append('n');
                    break;
                case 'о' :
                    strTran.append('o');
                    break;
                case 'п' :
                    strTran.append('p');
                    break;
                case 'р' :
                    strTran.append('r');
                    break;
                case 'с' :
                    strTran.append('s');
                    break;
                case 'т' :
                    strTran.append('t');
                    break;
                case 'у' :
                    strTran.append('y');
                    break;
                case 'ф' :
                    strTran.append('f');
                    break;
                case 'х' :
                    strTran.append('x');
                    break;
                case 'ц' :
                    strTran.append('c');
                    break;
                case 'ч' :
                    strTran.append('c');
                    strTran.append('h');
                    break;
                case 'ш' :
                case 'щ' :
                    strTran.append('s');
                    strTran.append('h');
                    break;
                case 'ь' :
                    strTran.append('\'');
                    break;
                case 'ю' :
                    strTran.append('y');
                    strTran.append('u');
                    break;
                case 'я' :
                    strTran.append('i');
                    strTran.append('a');
                    break;
                case ' ' :
                    strTran.append(' ');
                    break;
                default:
                    strTran.append(strRus.charAt(i));
                    break;
            }
        }
        System.out.println(strTran);
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        return scanner.nextLine();
    }
}
