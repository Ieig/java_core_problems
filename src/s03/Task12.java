package s03;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Task12 {
    public static void main(String[] args) {
        System.out.println("Введите строку:");
        String str = requestString();
        System.out.println("Введите разделитель:");
        String sep = requestString();
        Pattern pattern = Pattern.compile("[" + sep + "]");
        String[] words = pattern.split(str);

        for (String i : words) {
            System.out.println(i + " - " + i.length());
        }
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
