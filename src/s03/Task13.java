package s03;

import java.util.Scanner;

public class Task13 {
    public static void main(String[] args) {
        String origStr = requestString();
        StringBuilder finStr = new StringBuilder();
        String[] words = origStr.split(" ");
        for (String word : words) {
            if (word.length() > 0) {
                finStr.append(word);
                finStr.append(" ");
            }
        }
        finStr.deleteCharAt(finStr.length()-1);
        System.out.println(finStr);
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        return scanner.nextLine();
    }
}
