package s03;

import java.util.Scanner;

public class Task14 {
    public static void main(String[] args) {
        String str1 = requestString();
        String[] numbers = str1.split(" +");
        int sum = 0;

        for (int i = 0; i < numbers.length; i+=2) {
            sum += Integer.parseInt(numbers[i]);
        }

        System.out.println(sum);
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        return scanner.nextLine();
    }
}
