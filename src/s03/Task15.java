package s03;

import java.util.Scanner;

public class Task15 {
    public static void main(String[] args) {
        String str = requestString();
        int counter = 1;
        int maxCount = 0;
        int startsWith = 0;
        Character symbol = null;
        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) == str.charAt(i-1)) {
                counter++;
                if (maxCount < counter) {
                    maxCount = counter;
                    symbol = str.charAt(i);
                    startsWith = i - counter + 2;
                }
            }
            else {
                counter = 1;
            }
        }
        if (symbol == null) {
            System.out.println("Нет повторяющихся символов");
        }
        else {
            System.out.println(symbol);
            System.out.println(maxCount);
            System.out.println(startsWith);
        }
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку");
        return scanner.nextLine();
    }
}
