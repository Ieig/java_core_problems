package s03;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        int n = requestNumber();
        StringBuilder pin = new StringBuilder();

        for (int i = 0; i < n; i++) {
            pin.append((int)(Math.random() * 10));
        }
        System.out.println(pin);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
