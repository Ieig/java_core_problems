package s03;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        String str = requestString();
        for (int i = 0; i < str.length(); i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(str.substring(i, i+1));
            }
            System.out.println();
        }
    }
    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        return scanner.nextLine();
    }
}
