package s03;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        int a = requestNumber();
        int b = requestNumber();
        StringBuilder str1 = new StringBuilder();
        StringBuilder str2 = new StringBuilder();

        for (int i = 0; i < a; i++) {
            str1.append('*');
        }

        for (int i = 0; i < a; i++) {
            if (i == 0 || i == a - 1) {
                str2.append('*');
            }
            else {
                str2.append(' ');
            }
        }

        for (int i = 0; i < b; i++) {
            if (i == 0 || i == b - 1) {
                System.out.println(str1);
            }
            else {
                System.out.println(str2);
            }
        }
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
