package s03;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        String str = requestWord();
        int n = str.length();
        boolean areEqual = true;

        for (int i = 0; i < n/2; i++) {
            if (str.charAt(i) != str.charAt(n-1-i)) {
                areEqual = false;
                break;
            }
        }
        System.out.println(areEqual);
    }

    static String requestWord() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        return scanner.next();
    }
}
