package s03;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        String origStr = requestString();
        String origChar = requestString();

        int counter = 0;

        for (int i = 0; i < origStr.length(); i++) {
            if (origStr.charAt(i) == origChar.charAt(0)) {
                counter++;
            }
        }

        String finChar = origChar.toUpperCase();
        String finStr = origStr.replace(origChar, finChar);

        System.out.println(counter);
        System.out.println(finStr);
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        return scanner.nextLine();
    }
}
