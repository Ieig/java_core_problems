package s03;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        String origStr = requestString();
        StringBuilder finStr = new StringBuilder();
        for (int i = 0; i < origStr.length(); i++) {
            if (Character.isLowerCase(origStr.charAt(i))) {
                finStr.append(Character.toUpperCase(origStr.charAt(i)));
            }
            else {
                finStr.append(Character.toLowerCase(origStr.charAt(i)));
            }
        }
        System.out.println(finStr);
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        return scanner.nextLine();
    }
}
