package s03;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        String origStr = requestString();
        StringBuilder finalStr = new StringBuilder();

        for (int i = origStr.length()-1 ; i >= 0; i--) {
            finalStr.append(origStr.charAt(i));
        }
        System.out.println(finalStr);
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        return scanner.nextLine();
    }
}
