package s04;

public class Circle {
    private final double radius;
    private Double x;
    private Double y;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getSquare() {
        return radius * 2 * Math.PI;
    }

    public void setCenter(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void printInfo() {
        System.out.println("radius = " + radius + "; x = " + x + "; y = " + y);
    }

    public boolean isCrossing(Circle other) {
        return this.centerDistance(other) <= radius + other.radius;
    }

    public double centerDistance(Circle other) {
        if (this.x == null || other.x == null)  {
            System.out.println("Не установолены координаты центра одной из окружностей!!!");
            return Double.MAX_VALUE;
        }
        return Math.sqrt(Math.pow(x - other.x, 2) + Math.pow(y - other.y, 2));
    }
}
