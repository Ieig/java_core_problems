package s04;

import java.util.Collections;

public class Hero {
    private static final int INITIAL_HEALTH = 100;

    private int health;
    private final int power;
    private final int speed;

    Hero() {
        this.health = INITIAL_HEALTH;
        this.power = 50;
        this.speed = 50;
    }

    Hero(int speed) {
        this.health = INITIAL_HEALTH;
        this.speed = speed % 100;
        this.power = 100 - speed;
    }

    public int getHealth() {
        return health;
    }

    public int getPower() {
        return power;
    }

    public int getSpeed() {
        return speed;
    }

    public int hit(Hero enemy) {
        enemy.health -= power;
        return enemy.health;
    }

    public boolean isAlive() {
        return health > 0;
    }

    public void printStatus() {
        System.out.println(
                "health \t" + repeatAsterisk(health / 10) + "\n" +
                "power \t" + repeatAsterisk(power / 10) + "\n" +
                "speed \t" + repeatAsterisk(speed / 10 ) + "\n"
        );
    }

    private static String repeatAsterisk(int times) {
        return String.join("", Collections.nCopies(times, "*"));
    }
}
