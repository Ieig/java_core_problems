package s04;

public class Task1 {
    public static void main(String[] args) {
        Hero heroOne = new Hero(20);
        Hero heroTwo = new Hero(40);

        heroTwo.printStatus();
        heroOne.hit(heroTwo);
        heroTwo.printStatus();

        System.out.println("Is hero two still alive?\n " + heroTwo.isAlive());

        System.out.println("heroOne health = " + heroOne.getHealth());
        System.out.println("heroOne power = " + heroOne.getPower());
        System.out.println("heroOne speed = " + heroOne.getSpeed());
    }
}
