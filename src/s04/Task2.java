package s04;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        String str = requestString();
        StringBuilder result = new StringBuilder();
        stringRev(str, result);
        System.out.println(result);
    }

//    Что-то ничего лучше придумать я не смог. А требования к возвращаемому значению и параметрам метода определенные?
    static void stringRev(String orig, StringBuilder res) {
        if (res.length() == orig.length()) {
            return;
        }
        res.append(orig.charAt(orig.length() - res.length() - 1));
        stringRev(orig, res);
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку:");
        return scanner.nextLine();
    }
}
