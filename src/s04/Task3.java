package s04;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        int n = requestNumber();
        System.out.println(fibNumFinder(n));
    }

    static int fibNumFinder(int n) {
        if (n < 0) {
            System.out.println("Порядковый номер числа не может быть отрицательным");
            return -1;
        }
        if (n == 0) { return 0; }
        else if (n == 1) { return 1; }
        return fibNumFinder(n - 1) + fibNumFinder(n - 2);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите порядковый номер числа начиная с 0-го:");
        return scanner.nextInt();
    }
}
