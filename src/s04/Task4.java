package s04;

public class Task4 {
    public static void main(String[] args) {
        Circle circle1 = new Circle(0.71);
        System.out.println("Площаль circle1 = " + circle1.getSquare());
        circle1.setCenter(1, 1);

        System.out.println("circle1 info:");
        circle1.printInfo();

        Circle circle2 = new Circle(0.71);
        circle2.setCenter(2, 2);
        System.out.println("circle2 info:");
        circle2.printInfo();

        System.out.println("Пересечение окружностей circle1 и circle2:");
        System.out.println(circle2.isCrossing(circle1));

        System.out.println("Расстояние между circle1 и circle2 = " + circle1.centerDistance(circle2));
    }
}
