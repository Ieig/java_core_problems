package s05;

public class Addition extends Operation{

    @Override
    int calculate(int leftOperand, int rightOperand) {
        previous = current;
        current = leftOperand + rightOperand;
        return current;
    }
}
