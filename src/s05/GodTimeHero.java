package s05;

public class GodTimeHero extends Hero {

    private int godMinutes;
    private long timeStart ;
    private long timeFinish;

    GodTimeHero(int speed, int godMinutes) {
        super(speed);
        this.godMinutes = godMinutes;
        timeStart = System.currentTimeMillis();
        timeFinish = timeStart + godMinutes * 60 * 1000;
    }

    private boolean isStillGod() {
        if (timeFinish <= System.currentTimeMillis()) {
            return false;
        }
        return true;
    }

    @Override
    void healthLoss(int power) {
        if (!isStillGod()) {
            this.health -= power;
        }
    }
}
