package s05;

public class Multiplication extends Operation{

    @Override
    int calculate(int leftOperand, int rightOperand) {
        previous = current;
        current = leftOperand * rightOperand;
        return current;
    }
}
