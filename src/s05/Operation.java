package s05;

public abstract class Operation {
    int previous = 0;
    int current = 0;

    abstract int calculate(int leftOperand, int rightOperand);

    int returnPrevious() {
        return previous;
    }
}
