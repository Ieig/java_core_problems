package s05;

public class Task1 {
    public static void main(String[] args) throws InterruptedException {
        Hero hero1 = new Hero(30);
        GodTimeHero hero2 = new GodTimeHero(40, 1);

        System.out.println(hero2);
        hero1.hit(hero2);
        System.out.println(hero2);
        Thread.sleep(30000);
        hero1.hit(hero2);
        System.out.println(hero2);
        Thread.sleep(31000);
        hero1.hit(hero2);
        System.out.println(hero2);
    }
}
