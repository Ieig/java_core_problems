package s05;

public class Task2 {
    public static void main(String[] args) {
        Addition adding = new Addition();
        Multiplication multi = new Multiplication();

        int resultA = adding.calculate(2,4);
        int resultM = multi.calculate(2, 4);
        System.out.println(resultA);
        System.out.println(resultM);

        System.out.println(adding.calculate(3,7));
        System.out.println(multi.calculate(3, 7));
        System.out.println(adding.returnPrevious());
        System.out.println(multi.returnPrevious());
    }
}
