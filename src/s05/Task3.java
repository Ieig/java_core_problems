package s05;

public class Task3 {
    public static void main(String[] args) {
        ComplexNumber complex1 = new ComplexNumber(1, 3);
        ComplexNumber complex2 = new ComplexNumber(2, 4);
        ComplexNumber complex3 = new ComplexNumber(1, 3);

        System.out.println(complex1.equals(complex2));
        System.out.println(complex1.equals(complex3));

        System.out.println(complex1);
        System.out.println(complex2);
        System.out.println(complex3);
    }
}
