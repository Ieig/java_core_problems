package s05;

public class Task4 {
    public static void main(String[] args) {
        processRomanLetter(RomanLetter.J);
        processRomanLetter(RomanLetter.O);
        processRomanLetter(RomanLetter.T);
    }

    static void processRomanLetter(RomanLetter romanLetter) {
        System.out.println("Position of Letter " + romanLetter.name() + " is " + (romanLetter.getPosition1()));
        System.out.println("Position of Letter " + romanLetter.name() + " is " + (romanLetter.getPosition2()));
    }
}
