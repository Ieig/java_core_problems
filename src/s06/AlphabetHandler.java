package s06;

public class AlphabetHandler implements Handler{
    private static AlphabetHandler instance;

    private AlphabetHandler() {}

    public static AlphabetHandler getInstance() {
        if (instance == null) {
            instance = new AlphabetHandler();
        }
        return instance;
    }

    @Override
    public String handleMessage(String message) {
        if (message == null) {
            return null;
        }

        for (int i = 0; i < message.length(); i++) {
            if (Character.isLetter((message.charAt(i))) || message.charAt(i) == ' ') {}
            else return null;
        }
        return message;
    }
}
