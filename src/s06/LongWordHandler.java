package s06;

import java.util.regex.Pattern;

public class LongWordHandler implements Handler{
    private static LongWordHandler instance;
    private final int m;
    private final int n;

    private LongWordHandler(int m, int n) {
        this.m = m;
        this.n = n;
    }

    public static LongWordHandler getInstance(int m, int n) {
        if (instance == null) {
            instance = new LongWordHandler(m, n);
        }
        return instance;
    }

    @Override
    public String handleMessage(String message) {
        if (message == null) {
            return null;
        }

        Pattern pattern = Pattern.compile("\\s");
        String[] words = pattern.split(message);
        if (words.length < m) {
            return null;
        }
        for (String str : words) {
            if (str.length() < n) {
                return null;
            }
        }
        return message;
    }
}
