package s06;

public class LowerCaseHandler implements Handler{
    private static LowerCaseHandler instance;

    private LowerCaseHandler() {}

    public static LowerCaseHandler getInstance() {
        if (instance == null) {
            instance = new LowerCaseHandler();
        }
        return instance;
    }

    @Override
    public String handleMessage(String message) {
        if (message == null) {
            return null;
        }
        return message.trim().toLowerCase();
    }
}
