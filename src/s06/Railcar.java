package s06;

public class Railcar {
    private double loadCapacity;

    public Railcar(double loadCapacity) {
        this.loadCapacity = loadCapacity;
    }

    public void setLoadCapacity(double loadCapacity) {
        this.loadCapacity = loadCapacity;
    }

    public double getLoadCapacity() {
        return loadCapacity;
    }
}
