package s06;

public class Rectangle extends Shape {
    double width;
    double length;

    Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    @Override
    double calculatePerimeter() {
        return (width + length) * 2;
    }

    @Override
    double calculateArea() {
        return width * length;
    }
}
