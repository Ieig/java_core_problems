package s06;

public abstract class Shape {
    abstract double calculatePerimeter();
    abstract double calculateArea();
}
