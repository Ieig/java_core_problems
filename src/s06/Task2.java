package s06;

public class Task2 {
    public static void main(String[] args) {
        Shape[] figures = new Shape[3];
        figures[0] = new Square(5);
        figures[1] = new Rectangle(3, 7);
        figures[2] = new Triangle(6, 4, 4, 5);

        System.out.println(squareSum(figures));
    }
    static double squareSum(Shape figures[]) {
        double result = 0;
        for (Shape shape : figures) {
            result += shape.calculateArea();
        }
        return result;
    }
}