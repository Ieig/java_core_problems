package s06;

public class Task3 {
    public static void main(String[] args) {
        Train train1 = new Train(33.3, 66.7, 50.0, 50.0, 100, 0);

        System.out.println(train1.getTrainSize());
        System.out.println(train1.getTotalTrainCapacity());
        System.out.println(train1.getAvgRailcarCapacity());
    }
}
