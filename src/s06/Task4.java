package s06;

public class Task4 {
    public static void main(String[] args) {
        String message = "  mESSaGe One ";

        message = LowerCaseHandler.getInstance().handleMessage(message);
        message = AlphabetHandler.getInstance().handleMessage(message);
        message = LongWordHandler.getInstance(2, 3).handleMessage(message);

        if (message != null) {
            System.out.println(message);
        }
        else {
            System.out.println("Incorrect message");
        }
    }
}
