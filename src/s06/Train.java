package s06;

public class Train {
    private int trainSize;
    private Railcar[] railcars;

    public Train(double... carsWeight) {
        trainSize = carsWeight.length;
        railcars = new Railcar[trainSize];
        for (int i = 0; i < trainSize; i++) {
            railcars[i] = new Railcar(carsWeight[i]);
        }
    }

    public int getTrainSize() {
        return trainSize;
    }

    public double getTotalTrainCapacity() {
        double totalCap = 0;
        for (Railcar railcar : railcars) {
            totalCap += railcar.getLoadCapacity();
        }
        return totalCap;
    }

    public double getAvgRailcarCapacity() {
        return getTotalTrainCapacity() / trainSize;
    }
}
