package s06;

public class Triangle extends Shape {
    private double baseSide;
    private double leftSide;
    private double rightSide;
    private double height;

    Triangle(double baseSide, double leftSide, double rightSide, double height) {
        this.baseSide = baseSide;
        this.leftSide = leftSide;
        this.rightSide = rightSide;
        this.height = height;
    }

    @Override
    double calculatePerimeter() {
        return leftSide + rightSide + baseSide;
    }

    @Override
    double calculateArea() {
        return baseSide * height / 2;
    }
}
