package s07;

public interface Converter<T1, T2> {
    T2 convertingType(T1 obj);
}
