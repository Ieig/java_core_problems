package s07;

import java.time.LocalDate;

public class Driver {
    private String firstName;
    private String lastName;
    private String fathersName;
    private LocalDate dateOfBirth;
    private LocalDate dateIssued;
    private String licenseSer;
    private String licenseNum;

    public Driver(String firstName, String lastName, String fathersName, LocalDate dateOfBirth, LocalDate dateIssued,
                  String licenseSer, String licenseNum) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fathersName = fathersName;
        this.dateOfBirth = dateOfBirth;
        this.dateIssued = dateIssued;
        this.licenseSer = licenseSer;
        this.licenseNum = licenseNum;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFathersName() {
        return fathersName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public LocalDate getDateIssued() {
        return dateIssued;
    }

    public String getLicenseSer() {
        return licenseSer;
    }

    public String getLicenseNum() {
        return licenseNum;
    }
}
