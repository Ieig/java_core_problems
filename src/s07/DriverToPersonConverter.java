package s07;

public class DriverToPersonConverter implements Converter<Driver, Person>{
    public Person convertingType(Driver driver) {
        Person person = new Person(
                driver.getFirstName(),
                driver.getLastName(),
                driver.getFathersName(),
                driver.getDateOfBirth());
        return person;
    }
}
