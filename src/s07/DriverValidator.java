package s07;

import java.time.LocalDate;

public class DriverValidator implements Validator<Driver>{
    @Override
    public boolean isValid(Driver driver) {
        if (driver.getFirstName() == null || driver.getLastName() == null) {
            return false;
        }
        if (driver.getDateOfBirth() == null || driver.getDateIssued() == null) {
            return false;
        }
        if (driver.getLicenseSer() == null || driver.getLicenseNum() == null) {
            return false;
        }

        for (int i = 0; i < driver.getFirstName().length(); i++) {
            if (!Character.isLetter(driver.getFirstName().charAt(i))) {
                return false;
            }
        }
        for (int i = 0; i < driver.getLastName().length(); i++) {
            if (!Character.isLetter(driver.getLastName().charAt(i))) {
                return false;
            }
        }

        if (driver.getDateOfBirth().plusYears(18).isAfter(LocalDate.now())) {
            return false;
        }
        if (driver.getDateIssued().plusYears(10).isBefore(LocalDate.now())) {
            return false;
        }
        if (driver.getLicenseSer().length() != 4 && driver.getLicenseNum().length() != 6) {
            return false;
        }
        return true;
    }
}
