package s07;

import java.time.LocalDate;

public class Person {
    private String firstName;
    private String lastName;
    private String fathersName;
    private LocalDate dateOfBirth;

    public Person(String firstName, String lastName, String fathersName, LocalDate dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fathersName = fathersName;
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFathersName() {
        return fathersName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }
}
