package s07;

import java.time.LocalDate;

public class Task2 {
    public static void main(String[] args) {
        Driver driver = new Driver("Daniel", "Plainview", null,
                LocalDate.of(2004, 1, 1), LocalDate.of(2012, 12, 31),
                "7803", "203830");

        DriverToPersonConverter dtp = new DriverToPersonConverter();
        Person person = dtp.convertingType(driver);
    }
}
