package s07;

import java.math.BigDecimal;

public class Task3 {
    public static void main(String[] args) {
        double m = 1_234_567_890_123_456_000.;
        long n = 1_234_567_890_123_456_001L;
        double o = 1_234_567_890_123.1;
        long p = 1_234_567_890_123L;
        System.out.println(compareNumbers(m, n));
        System.out.println(compareNumbers(o, p));
    }

    public static <T1 extends Number, T2 extends Number> int compareNumbers(T1 a, T2 b) {
        BigDecimal one = new BigDecimal(a.toString());
        BigDecimal two = new BigDecimal(b.toString());

        return one.compareTo(two);
    }
}
