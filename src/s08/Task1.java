package s08;

import java.io.*;

public class Task1 {
    public static void main(String[] args) {
        try {
            File input = new File("C:\\Users\\Игорь\\IdeaProjects\\JBorn_Core_11_Lebedev\\src\\s08\\input.txt");
            File output = new File("C:\\Users\\Игорь\\IdeaProjects\\JBorn_Core_11_Lebedev\\src\\s08\\output.txt");
            sequenceCalculator(input, output);
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }
    static void sequenceCalculator(File input_file, File output_file) throws Exception {
        try {
            String inFileFormat = "txt";
            validFileFormat(input_file, inFileFormat);
            String input_str = readFirstLine(input_file);
            String[] numbers = input_str.split(" ");
            Long result = 0L;
            for (String word : numbers) {
                Long number = Long.parseLong(word);
                result = Math.addExact(result, number);
            }
            String output_str = result.toString();
            writeToFirst(output_file, output_str);
        } catch (IOException e) {
            throw new IOException("Problems reading input file!", e);
        } catch (ArithmeticException | NumberFormatException e) {
            throw new RuntimeException("Problems with the format/processing data!", e);
        }
    }

    static String readFirstLine(File file) throws IOException {
        try (FileInputStream stream = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
            return reader.readLine();
        }
    }

    static void writeToFirst(File file, String value) throws IOException {
        FileWriter writer = new FileWriter(file, false);
        writer.write(value);
        writer.flush();
        writer.close();
    }

    static void validFileFormat(File file, String str) throws FileBadFormatException{
        String fileName = file.toString();
        System.out.println(fileName);
        if (!fileName.substring(fileName.length()-str.length()).equals(str)) {
            throw new FileBadFormatException("Wrong file extension");
        }
    }

    public static class FileBadFormatException extends IOException {
        public FileBadFormatException (String message) {
            super(message);
        }
    }
}
