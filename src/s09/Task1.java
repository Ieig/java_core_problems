package s09;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Task1 {
    public static void main(String[] args) {
        Path baseDir = Paths.get("C:\\Users\\Игорь\\IdeaProjects\\JBorn_Core_11_Lebedev");
        Path[] result = findFile(baseDir, "Task1.java");
        for (Path path : result) {
            System.out.println(path);
        }
    }

    public static Path[] findFile(Path basedir, String fileName) {
        Path[] result = new Path[0];
        try (Stream<Path> paths = Files.walk(basedir)) {
            result = paths.filter(path -> path.toString().contains(fileName))
                    .toArray(Path[]::new);
        } catch (IOException e) {
            e.printStackTrace();

        }
        return result;
    }
}
