package s09;

import java.io.*;
import java.util.Random;

public class Task2 {
    static final String PATH_TO_ALL = "src/s09/numbers.txt";
    static final String PATH_TO_POS = "src/s09/positive_numbers.txt";
    static final String PATH_TO_NEG = "src/s09/negative_numbers.txt";

    public static void main(String[] args) {
        writeRandomNumbersToFile(PATH_TO_ALL, 200);
        int[] array = readNumbersFromFile(PATH_TO_ALL);
        separateAndWriteNumbersToFiles(array, PATH_TO_POS, PATH_TO_NEG);
    }

    public static void writeRandomNumbersToFile(String path, int amount) {
        Random rand = new Random();
        try (FileWriter fw = new FileWriter(PATH_TO_ALL)) {
            for (int i = 0; i < amount; i++) {
                if (i % 2 == 0) {
                    fw.write(String.valueOf(rand.nextInt(Integer.MAX_VALUE)));
                } else {
                    fw.write(String.valueOf(rand.nextInt(Integer.MAX_VALUE) * -1));
                }
                if (i < amount - 1) {
                    fw.write(" ");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int[] readNumbersFromFile(String path) {
        int[] numbers;
        int size = 0;
        String text = "";
        try (FileReader fis = new FileReader(path);
             BufferedReader br = new BufferedReader(fis)) {
            text = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] words = text.split(" ");
        numbers = new int[words.length];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(words[i]);
        }
        return numbers;
    }

    public static void separateAndWriteNumbersToFiles(int[] array, String pathPos, String pathNeg) {
        try (FileWriter fw1 = new FileWriter(pathPos);
             FileWriter fw2 = new FileWriter(pathNeg)) {
            for (int i = 0; i < array.length; i++) {
                if (array[i] >= 0) {
                    fw1.write(String.valueOf(array[i]));
                    if (i != array.length - 1) {
                        fw1.write(" ");
                    }
                } else {
                    fw2.write(String.valueOf(array[i]));
                    if (i != array.length - 1) {
                        fw2.write(" ");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
