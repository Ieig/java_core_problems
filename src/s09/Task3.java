package s09;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Task3 {
    public static void main(String[] args) {
        try (InputStream input = new FileInputStream("src/s09/in.txt");) {
            System.out.println(readAsString(input, StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readAsString(InputStream inputStream, Charset charset) throws IOException {
        byte[] data = new byte[inputStream.available()];
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream)) {
            bufferedInputStream.read(data);
            return new String(data, charset);
        } catch (IOException e) {
            throw new IOException("Problems reading input stream");
        }
    }
}
