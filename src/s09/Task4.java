package s09;

import java.io.*;

public class Task4 {
    public static String path = "src/s09/userdata.ser";

    public static void main(String[] args) {
        User user = new User("Ivan", "Ivanov");
        System.out.println(user);

        try (FileOutputStream fileOutputStream = new FileOutputStream(path);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(user);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileInputStream fileInputStream = new FileInputStream(path);
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            System.out.println("Deserialization starts...");
            User deserialized = (User) objectInputStream.readObject();
            System.out.println(deserialized);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
