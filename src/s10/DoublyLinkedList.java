package s10;

import java.util.NoSuchElementException;

public class DoublyLinkedList<T> implements List<T>{
    private int size;
    private Node<T> first;
    private Node<T> last;

    public DoublyLinkedList () {
    }

    @Override
    public T get(int i) {
        if (size == 0) {
            throw new NoSuchElementException("List is empty");
        }
        isRealIndex(i);

        return getToNode(i).item;
    }

    private Node<T> getToNode(int i) {
        Node<T> newNode;
        if (i <= (size >> 1)) {
            newNode = first;
            for (int j = 0; j < i; j++) {
                newNode = newNode.next;
            }
        } else {
            newNode = last;
            for (int j = size - 1; j > i; j--) {
                newNode = newNode.prev;
            }
        }
        return newNode;
    }

    @Override
    public void put(T e) {
        linkLast(e);
    }

    @Override
    public void put(int i, T e) {
        isRealIndex(i);
        if (size == 0) {
            linkFirst(e);
        } else {
            linkBefore(getToNode(i), e);
        }
    }

    private void linkFirst(T e) {
        Node<T> oldFirst = first;
        Node<T> newFirst = new Node<T>(null, e, oldFirst);
        first = newFirst;
        if (oldFirst == null) {
            last = newFirst;
        } else {
            oldFirst.prev = newFirst;
        }
        size++;
    }

    private void linkLast(T e) {
        Node<T> oldLast = last;
        Node<T> newLast = new Node<T>(last, e, null);
        last = newLast;
        if (oldLast == null) {
            first = newLast;
        } else {
            oldLast.next = newLast;
        }
        size++;
    }

    private void linkBefore(Node<T> beNext, T e) {
        Node<T> bePrev = beNext.prev;
        Node<T> newNode = new Node<>(bePrev, e, beNext);
        if (bePrev == null) {
            first = newNode;
        } else {
            bePrev.next = newNode;
        }
        beNext.prev = newNode;
        size++;
    }

    @Override
    public void remove(int i) {
        if (size == 0) {
            throw new NoSuchElementException("List is empty");
        }
        isRealIndex(i);
        unlink(getToNode(i));
    }

    @Override
    public void remove(T e) {
        if (length() == 0) {
            throw new NoSuchElementException("List is empty");
        }
        if (e == null) {
            for (Node<T> tmp = first; tmp != null; tmp = tmp.next) {
                if (tmp.item == null) {
                    unlink(tmp);
                }
            }
        } else {
            for (Node<T> tmp = first; tmp != null; tmp = tmp.next) {
                if (tmp.item.equals(e)) {
                    unlink(tmp);
                }
            }
        }
    }

    private void unlink(Node<T> toErase) {
        Node<T> prev = toErase.prev;
        Node<T> next = toErase.next;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            toErase.prev = null;
        }

        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            toErase.next = null;
        }

        toErase.item = null;
        size--;
    }

    @Override
    public int length() {
        // Если бы не было int size
        int counter;
        if (first == null) {
            counter = 0;
        } else {
            Node<T> tmp = first;
            counter = 1;
            while (tmp.next != null) {
                tmp = tmp.next;
                counter++;
            }
        }
        return counter;
    }

    private void isRealIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index " + index + " is out of size bounds: " + size);
        }
    }

    private static class Node<T> {
        T item;
        private Node<T> prev;
        private Node<T> next;

        Node(Node<T> prev, T item, Node<T> next) {
            this.item = item;
            this.prev = prev;
            this.next = next;
        }
    }
}
