package s10;

import java.util.NoSuchElementException;

// Не будем принимать null-значения
public class LinkedQueue<T> implements Queue<T> {
    private Node<T> head;
    private Node<T> tail;

    LinkedQueue() {
    }

    @Override
    public boolean add(T e) {
        if (e == null) {
            throw new NullPointerException("can't add null-value into the queue");
        }
        Node<T> newNode = new Node<>(e);
        if (head == null) {
            head = tail = newNode;
        } else {
            tail = tail.next = newNode;
        }
        return true;
    }

    @Override
    public T poll() {
        if (head == null) {
            return null;
        }
        Node<T> oldHead = head;
        if (head != tail) {
            Node<T> newHead = oldHead.next;
            head = newHead;
        } else {
            head = null;
            tail = null;
        }

        oldHead.next = oldHead;
        T e = oldHead.item;
        oldHead.item = null;
        return e;
    }

    @Override
    public T peek() {
        return head == null ? null : head.item;
    }

    private static class Node<T> {
        T item;
        Node<T> next;

        Node(T item) {
            this.item = item;
        }
    }
}
