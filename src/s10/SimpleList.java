package s10;

public class SimpleList<E> {
    int size = 0;
    Node<E> head;
    Node<E> last;

    public SimpleList() {
    }

    public void add(E e) {
        if (size == 0) {
            head = new Node<>(e, null);
            last = head;
        } else {
            Node<E> newNode = new Node<>(e, null);
            last.next = newNode;
            last = newNode;
        }
        size++;
    }

    public void add(E e, Node<E> next) {
        if (size == 0) {
            head = new Node<>(e, next);
            last = head;
        } else {
            Node<E> newNode = new Node<>(e, next);
            last.next = newNode;
            last = newNode;
        }
        size++;
    }

    public Node<E> nodeAt(int index) {
        if (index == 0) {
            return head;
        } else if (index > 0 && index < size) {
            int counter = 0;
            Node<E> itNode = head;
            while (counter != index) {
                itNode = itNode.next;
                counter++;
            }
            return itNode;
        } else {
            throw new IndexOutOfBoundsException("wrong index");
        }
    }

    public static class Node<E> {
        E item;
        Node<E> next;

        Node(E element, Node<E> next) {
            this.item = element;
            this.next = next;
        }
    }
}
