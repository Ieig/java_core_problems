package s10;

public class Task1 {
    public static void main(String[] args) {
        SimpleList<String> list  = new SimpleList<>();
        list.add("first");
        list.add("second");
        list.add("third");
        list.add("fourth");
        list.add("fifth", list.nodeAt(1));

        System.out.println("List is cycled: " + isCycled(list));
    }

    public static <E> boolean isCycled(SimpleList<E> list) {
        SimpleList.Node<E> slow = list.head;
        SimpleList.Node<E> fast = list.head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;

            if (slow == fast) {
                return true;
            }
        }
        return false;
    }
}
