package s10;

public class Task2 {
    public static void main(String[] args) {
        DoublyLinkedList<String> list = new DoublyLinkedList<>();
        list.put("one");
        list.put("two");
        list.put("three");
        list.put("six");
        System.out.println(list.length());
        list.remove("six");
        System.out.println(list.length());
        System.out.println(list.get(2));
        list.put(0, "new first");
        list.put(3, "new inner");
        System.out.println(list.get(0));
        System.out.println(list.get(3));
        System.out.println(list.length());
        list.remove(0);
        System.out.println(list.length());
        list.remove(3);
        for (int i = 0; i < list.length(); i++) {
            System.out.println(list.get(i));
        }
    }
}
