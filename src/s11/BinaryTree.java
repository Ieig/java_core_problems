package s11;

public class BinaryTree<E extends Comparable<E>> {
    private TreeNode<E> root;

    public BinaryTree() {
    }

    public void print(Order order) {
        if (root == null) {
            System.out.println("Tree is empty!");
        } else {
            switch (order) {
                case PREORDER -> preOrderPrint(root);
                case INORDER -> inOrderPrint(root);
                case POSTORDER -> postOrderPrint(root);
            }
            System.out.println();
        }
    }

    private void inOrderPrint(TreeNode<E> node) {
        if (node == null) {
            return;
        }
        inOrderPrint(node.left);
        System.out.print(node.item + " ");
        inOrderPrint(node.right);
    }

    private void preOrderPrint(TreeNode<E> node) {
        if (node == null) {
            return;
        }
        System.out.print(node.item + " ");
        inOrderPrint(node.left);
        inOrderPrint(node.right);
    }

    private void postOrderPrint(TreeNode<E> node) {
        if (node == null) {
            return;
        }
        inOrderPrint(node.left);
        inOrderPrint(node.right);
        System.out.print(node.item + " ");
    }

    public boolean contains(E value) {
        if (value == null || root == null) {
            throw new NullPointerException();
        }
        TreeNode<E> current = root;
        while (current.item != value) {
            if (value.compareTo(current.item) < 0) {
                current = current.left;
            } else {
                current = current.right;
            }
            if (current == null) {
                return false;
            }
        }
        return true;
    }

    public boolean add(E value) {
        if (value == null) {
            throw new NullPointerException();
        }
        TreeNode<E> newNode = new TreeNode<>(value);
        if (root == null) {
            root = newNode;
            return true;
        } else {
            TreeNode<E> current = root;
            TreeNode<E> parent;
            while (true) {
                parent = current;
                if (value.compareTo(current.item) < 0) {
                    current = current.left;
                    if (current == null) {
                        parent.left = newNode;
                        return true;
                    }
                } else if (value.compareTo(current.item) > 0) {
                    current = current.right;
                    if (current == null) {
                        parent.right = newNode;
                        return true;
                    }
                } else {
                    return false;
                }
            }
        }
    }

    public boolean remove(E value) {
        if (value == null || root == null) {
            throw new NullPointerException();
        }
        TreeNode<E> parent = root;
        TreeNode<E> current = root;

        boolean isLeft = true;
        while (current.item.compareTo(value) != 0) {
            parent = current;
            if (value.compareTo(current.item) < 0) {
                current = current.left;
                isLeft = true;
            } else {
                current = current.right;
                isLeft = false;
            }
            if (current == null) {
                return false;
            }
        }

        if (current.left == null && current.right == null) {
            if (current == root) {
                root = null;
            } else if (isLeft) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        } else if (current.right == null) {
            if (current == root) {
                root = current.left;
            } else if (isLeft) {
                parent.left = current.left;
            } else {
                parent.right = current.left;
            }
        } else if (current.left == null) {
            if (current == root) {
                root = current.right;
            } else if (isLeft) {
                parent.left = current.right;
            } else {
                parent.right = current.right;
            }
        } else {
            TreeNode<E> successor = getSuccessor(current);
            if (current == root) {
                root = successor;
            } else if (isLeft) {
                parent.left =successor;
            } else {
                parent.right = successor;
            }
            successor.left = current.left;
        }
        return true;
    }

    private TreeNode<E> getSuccessor(TreeNode<E> toDelete) {
        TreeNode<E> successor = toDelete.right;
        TreeNode<E> successorParent = toDelete;
        while (successor.left != null) {
            successorParent = successor;
            successor = successor.left;
        }
        if (successor != toDelete.right) {
            successorParent.left = successor.right;
            successor.right = toDelete.right;
        }
        return successor;
    }

    private static class TreeNode<E> {
        E item;
        TreeNode<E> left;
        TreeNode<E> right;

        TreeNode(E item) {
            this.item = item;
        }
    }

    public enum Order {
        PREORDER,
        INORDER,
        POSTORDER,
    }
}
