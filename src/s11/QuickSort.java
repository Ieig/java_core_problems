package s11;

public class QuickSort {
    public static <E extends Comparable<E>> void sort(E[] array) {
        sort(array, 0, array.length - 1);
    }

    public static <E extends Comparable<E>> void sort(E[] array, int first, int last) {
        if (first >= last) {
            return;
        }
        int pivot = partition(array, first, last);
        sort(array, first, pivot - 1);
        sort(array, pivot + 1, last);
    }

    private static <E extends Comparable<E>> int partition(E[] array, int first, int last) {
        int left = first;
        int right = last;
        while (left < right) {
            while (array[left].compareTo(array[last]) <= 0 && left < right) {
                left++;
            }
            while (array[right].compareTo(array[last]) >= 0 && left < right) {
                right--;
            }
            swap(array, left, right);
        }
        swap(array, left, last);

        return left;
    }

    private static <E> void swap(E[] array, int left, int right) {
        if (left == right) {
            return;
        }
        E temp = array[left];
        array[left] = array[right];
        array[right] = temp;
    }
}
