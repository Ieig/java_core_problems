package s11;

import java.util.Random;

public class Task1 {
    public static void main(String[] args) {
        Integer[] array = new Integer[100];
        Random rand = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt(100);
        }
        System.out.println(isSorted(array));
        QuickSort.sort(array);
        System.out.println(isSorted(array));
    }

    private static <E extends Comparable<E>> boolean isSorted(E[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i].compareTo(array[i + 1]) > 0) {
                return false;
            }
        }
        return true;
    }
}
