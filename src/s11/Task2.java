package s11;

public class Task2 {
    public static void main(String[] args) {
        BinaryTree<Integer> tree = new BinaryTree<>();
        tree.print(BinaryTree.Order.INORDER);
        System.out.println(tree.add(7));
        System.out.println(tree.add(3));
        System.out.println(tree.add(11));
        System.out.println(tree.add(1));
        System.out.println(tree.add(4));
        System.out.println(tree.add(6));
        System.out.println(tree.add(9));
        System.out.println(tree.add(10));
        System.out.println(tree.add(10));
        System.out.println(tree.add(13));
        System.out.println(tree.add(13));
        System.out.println(tree.contains(6));
        tree.print(BinaryTree.Order.INORDER);
        System.out.println(tree.remove(7));
        System.out.println(tree.remove(11));
        System.out.println(tree.remove(12));
        tree.print(BinaryTree.Order.INORDER);
    }
}
