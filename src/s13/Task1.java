package s13;

import java.util.ArrayList;

public class Task1 {
    public static void main(String[] args) {
        ArrayList<EmergencyContact> list = new ArrayList<>();
        list.add(new EmergencyContact("Will", "1234567"));
        list.add(new EmergencyContact("Tom", "7654321"));

        DriverCard goodOne = new DriverCard("Pam",
                21L,  0, "abra@cadab.ra", list);
        DriverCard badName = new DriverCard(null,
                23L,  2, "abra@cadab.ra", list);
        DriverCard badAge = new DriverCard("Jack",
                17L, 2, "abra@cadab.ra", list);
        DriverCard badAccidents = new DriverCard("Sam",
                19L,  2, "abra@cadab.ra", list);
        DriverCard badMail = new DriverCard("John",
                25L, 2, "abra@cadabra.", list);
        DriverCard badContact = new DriverCard("Bob",
                22,  3, "abra@cadab.ra", list);

        Validator validator = new Validator();
        System.out.println("1");
        System.out.println(validator.validate(goodOne));
        System.out.println("2");
        System.out.println(validator.validate(badName));
        System.out.println("3");
        System.out.println(validator.validate(badAge));
        System.out.println("4");
        System.out.println(validator.validate(badAccidents));
        System.out.println("5");
        System.out.println(validator.validate(badMail));
        System.out.println("6");
        list.clear();
        System.out.println(validator.validate(badContact));
    }
}
