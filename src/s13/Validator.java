package s13;

import java.lang.reflect.Field;
import java.util.Collection;

public class Validator {
    boolean validate(Object obj) {
        Class tClass = obj.getClass();
        for (Field field : tClass.getDeclaredFields()) {
            if (!field.canAccess(obj)) {
                field.setAccessible(true);
            }
            if (field.isAnnotationPresent(Min.class)) {
                if (!validateMin(field, obj)) {
                    return false;
                }
            }
            if (field.isAnnotationPresent(Max.class)) {
                if (!validateMax(field, obj)) {
                    return false;
                }
            }
            if (field.isAnnotationPresent(Regexp.class)) {
                if (!validateRegexp(field, obj)) {
                    return false;
                }
            }
            if (field.isAnnotationPresent(NotNull.class)) {
                if (!validateNotNull(field, obj)) {
                    return false;
                }
            }
            if (field.isAnnotationPresent(NotEmpty.class)) {
                if (!validateNotEmpty(field, obj)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean validateMin(Field field, Object obj) {
        Min min = field.getAnnotation(Min.class);
        int minValue = min.length();
        Double value = fetchDouble(field, obj);
        if (value != null) {
            return value >= minValue;
        }
        return true;
    }

    private boolean validateMax(Field field, Object obj) {
        Max max = field.getAnnotation(Max.class);
        int maxValue = max.length();
        Double value = fetchDouble(field, obj);
        if (value != null) {
            return value <= maxValue;
        }
        return true;
    }

    private boolean validateRegexp(Field field, Object obj) {
        Regexp regexp = field.getAnnotation(Regexp.class);
        String expr = regexp.regexp();
        try {
            Object o = field.get(obj);
            if (o instanceof String str) {
                return str.matches(expr);
            } else  {
                System.out.println("wrong annotation usage");
            }
        } catch (IllegalAccessException e) {
            System.out.println("Exception getting value from the field: " + field.getName());
        }
        return true;
    }

    private boolean validateNotNull(Field field, Object obj) {
        Object o = null;
        try {
            o = field.get(obj);
        } catch (IllegalAccessException e) {
            System.out.println("Exception getting value from the field: " + field.getName());
        }
        return !(o == null);
    }

    private boolean validateNotEmpty(Field field, Object obj) {
        try {
            Object o = field.get(obj);
            if (o instanceof Collection collection) {
                return !collection.isEmpty();
            } else {
                System.out.println("wrong annotation usage");
            }
        } catch (IllegalAccessException e) {
            System.out.println("Exception getting value from the field: " + field.getName());
        }
        return true;
    }

    private Double fetchDouble(Field field, Object obj) {
        Double value = null;
        try {
            Object o = field.get(obj);
            if (o instanceof Number number) {
                value = number.doubleValue();
            } else  {
                System.out.println("wrong annotation usage");
            }
        } catch (IllegalAccessException e) {
            System.out.println("Exception getting value from the field: " + field.getName());
        }
        return value;
    }
}
