package s14;

import java.util.Comparator;

public class CounterComparator implements Comparator<CounterRunnable> {

    @Override
    public int compare(CounterRunnable o1, CounterRunnable o2) {
        return Integer.compare(o2.getResult(), o1.getResult());
    }
}
