package s14;

class CounterRunnable implements Runnable {
    private final String text;
    private final char c;
    private int result;

    public CounterRunnable(String text, Character c) {
        this.text = text;
        this.c = c;
        this.result = 0;
    }

    public int getResult() {
        return result;
    }

    @Override
    public void run() {
        for (int i = 0; i < text.length(); i++) {
            if (c == text.charAt(i) || Character.toUpperCase(c) == text.charAt(i)) {
                result++;
            }
        }
    }

    @Override
    public String toString() {
        return "Letter " + c + "; quantity: " + result;
    }
}
