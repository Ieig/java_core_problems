package s14;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Task1 {
    private static final String PATH = "src/s14/text.txt";
    private static final String CHARSET_STR = "UTF-8";
    private static final Charset CHARSET = Charset.forName(CHARSET_STR);

    public static void main(String[] args) throws UnsupportedEncodingException {
        String text = readText(PATH, CHARSET);

        CounterRunnable[] counter = letterMultiCounter(text);

        try (PrintStream ps = new PrintStream(System.out, true, CHARSET_STR)) {
            Arrays.sort(counter, new CounterComparator());
            for (CounterRunnable cr : counter) {
                ps.println(cr);
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println("Encoding not supported!");
        }
    }

    private static String readText(String path, Charset charset) {
        StringBuilder strings = new StringBuilder();
        String text;
        try (BufferedReader br = Files.newBufferedReader(Path.of(path), charset)) {
            while ((text = br.readLine()) != null) {
                strings.append(text);
            }
        } catch (IOException e) {
            System.out.println("Problems while accessing or reading from file");
        }
        return new String(strings);
    }

    private static CounterRunnable[] letterMultiCounter(String text) {
        CounterRunnable[] counter = new CounterRunnable[33];
        for (int i = 0; i < 32; i++) {
            counter[i] = new CounterRunnable(text, (char) ('а' + i));
        }
        counter[32] = new CounterRunnable(text, (char) ('а' + 33));

        try {
            Thread[] threads = new Thread[counter.length];
            for (int i = 0; i < counter.length; i++) {
                Thread thread = new Thread(counter[i]);
                thread.start();
                threads[i] = thread;
            }
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }
        return counter;
    }
}
