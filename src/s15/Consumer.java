package s15;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {
    BlockingQueue<Message> queue;
    int sleepTime;

    public Consumer(BlockingQueue<Message> queue, int sleepTime) {
        this.queue = queue;
        this.sleepTime = sleepTime;
    }

    @Override
    public void run() {
        while (true) {
            try {
                consume(queue.take());
                Thread.sleep(sleepTime);
                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    private void consume(Message message) {
        System.out.println("Hello, " + message.getName());
    }
}
