package s15;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {
    BlockingQueue<Message> queue;
    Message message;
    int sleepTime;

    public Producer(BlockingQueue<Message> queue, int sleepTime) {
        this.queue = queue;
        this.sleepTime = sleepTime;
    }

    @Override
    public void run() {
        int i = 0;
        while (true) {
            try {
                queue.put(produce(i++));
                Thread.sleep(sleepTime);
                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    private Message produce(int i) {
        return new Message(String.valueOf(i));
    }
}
