package s15;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Task1 {
    private static final int PROD_AMOUNT = 2;
    private static final int CONS_AMOUNT = 2;

    public static void main(String[] args) {
        ArrayBlockingQueue<Message> queue = new ArrayBlockingQueue<>(100, true);
        Producer[] producers = new Producer[PROD_AMOUNT];
        Consumer[] consumers = new Consumer[CONS_AMOUNT];
        try {
            producers = getRunnableArray(Producer.class, PROD_AMOUNT, queue, 1000);
            consumers = getRunnableArray(Consumer.class, CONS_AMOUNT, queue, 500);
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
        Thread[] threads = startQueue(producers, consumers);

        stopQueue(threads);
    }

    private static Thread[] startQueue(Producer[] producers, Consumer[] consumers) {
        Thread[] threads = new Thread[PROD_AMOUNT + CONS_AMOUNT];
        for (int i = 0; i < threads.length; i++) {
            Thread thread;
            if (i < PROD_AMOUNT) {
                thread = new Thread(producers[i]);
            } else {
                thread = new Thread(consumers[i - PROD_AMOUNT]);
            }
            thread.start();
            threads[i] = thread;
        }
        return threads;
    }

    private static void stopQueue(Thread[] threads) {
        for (Thread thread : threads) {
            try {
                thread.join(10000);
                if (thread.isAlive()) {
                    thread.interrupt();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static <T extends Runnable> T[] getRunnableArray
            (Class<T> clazz, int amount, BlockingQueue<Message> queue, int sleepTime)
            throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        T[] array = (T[]) Array.newInstance(clazz, amount);
        Constructor<T> constructor = clazz.getConstructor(BlockingQueue.class, int.class);
        for (int i = 0; i < amount; i++) {
            array[i] = constructor.newInstance(queue, sleepTime);
        }
        return array;
    }
}
