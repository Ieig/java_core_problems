package s16;

import s13.*;

import java.util.ArrayList;

public class DriverCard {
    @NotNull
    private String fullName;

    @Min(length = 21)
    private long age;

    @Min(length = 7)
    private double rating;

    @Max(length = 3)
    private int numOfAccidents;

    @NotNull
    @Regexp(regexp = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")
    private String email;

    @NotEmpty
    private ArrayList<EmergencyContact> contacts;

    public DriverCard(String fullName, long age, int numOfAccidents,
                      String email, ArrayList<EmergencyContact> contacts) {
        this.fullName = fullName;
        this.age = age;
        this.numOfAccidents = numOfAccidents;
        this.email = email;
        this.contacts = contacts;
        this.rating = 7.1;
    }

    public String getFullName() {
        return fullName;
    }

    public long getAge() {
        return age;
    }

    public double getRating() {
        return rating;
    }

    public int getNumOfAccidents() {
        return numOfAccidents;
    }

    public String getEmail() {
        return email;
    }

    public ArrayList<EmergencyContact> getContacts() {
        return contacts;
    }

    @Override
    public String toString() {
        return "Full name: " + fullName +
                "; Age: " + age +
                "; Accidents: " + numOfAccidents +
                "; E-mail: " + email;
    }
}
