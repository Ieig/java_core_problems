package s16;

import s13.EmergencyContact;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Task1 {
    public static void main(String[] args) {
        ArrayList<EmergencyContact> list1 = new ArrayList<>();
        ArrayList<EmergencyContact> list2 = new ArrayList<>();
        list1.add(new EmergencyContact("Will", "1234567"));
        list1.add(new EmergencyContact("Tom", "7654321"));

        List<DriverCard> drivers = new ArrayList<>();
        drivers.add(new DriverCard("Pam", 21L, 0, "abra@cadab.ra", list1));
        drivers.add(new DriverCard(null, 23L,  2, "abra@cadab.ra", list1));
        drivers.add(new DriverCard("Jack", 17L, 2, "abra@cadab.ra", list1));
        drivers.add(new DriverCard("Sam", 19L,  2, "abra@cadab.ra", list1));
        drivers.add(new DriverCard("John", 25L, 2, "abra@cadabra.", list1));
        drivers.add(new DriverCard("Bob", 22,  3, "abra@cadab.ra", list2));

        Validator<DriverCard> validator = (driverCard) -> {
            return driverCard.getFullName() != null
                    && driverCard.getAge() >= 21
                    && driverCard.getRating() >= 7
                    && driverCard.getNumOfAccidents() <= 3
                    && driverCard.getEmail() != null
                    && driverCard.getEmail().matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")
                    && !driverCard.getContacts().isEmpty();
        };

        List<DriverCard> validatedDrivers = drivers.stream()
                .filter(validator::validate)
                .collect(Collectors.toList());

        for (DriverCard driverCard : validatedDrivers) {
            System.out.println(driverCard);
        }
    }
}
