package s16;

import java.util.Arrays;
import java.util.Random;

public class Task2 {
    public static void main(String[] args) {
        int[] array = new int[1_000_000_000];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(2_000_001) - 1_000_000;
        }
        int[] amount = { 1_000, 1_000_000, 1_000_000_000 };
        for (int i = 0; i < amount.length; i++) {
            System.out.println("Array size = " + amount[i]);
            timingStream(Arrays.copyOf(array, amount[i]));
            timingParallelStream(Arrays.copyOf(array, amount[i]));
            System.out.println();
        }
    }

    private static void timingStream(int[] array) {
        long start = System.currentTimeMillis();
        long streamSumSubsequent = Arrays.stream(array)
                .mapToLong(Long::valueOf)
                .filter(i -> i > 0)
                .sum();
        long end = System.currentTimeMillis();
        System.out.println(streamSumSubsequent);
        System.out.println("Time for single thread:\t" + (end - start));
    }

    private static void timingParallelStream(int[] array) {
        long start = System.currentTimeMillis();
        long streamSumParallel = Arrays.stream(array)
                .parallel()
                .mapToLong(Long::valueOf)
                .filter(i -> i > 0)
                .sum();
        long end = System.currentTimeMillis();
        System.out.println(streamSumParallel);
        System.out.println("Time for multi thread:\t" + (end - start));
    }
}
