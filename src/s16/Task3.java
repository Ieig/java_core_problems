package s16;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class Task3 {
    public static void main(String[] args) {
        Integer[] array = new Integer[10];

        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(5) - 2;
        }
        System.out.println(Arrays.toString(array));

        int sum = IntStream.range(0, array.length)
                .filter(i -> i % 2 == 0)
                .map(i -> array[i])
                .sum();

        System.out.println(sum);
    }
}
