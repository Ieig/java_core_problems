package s16;

import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Task4 {
    public static void main(String[] args) {
        List<List<String>> list = Arrays.asList(
                Arrays.asList("Ваня", "Аня", null, "Александр"),
                Arrays.asList("Дима", null, "Артемида", "Коля"),
                Arrays.asList("Антон", "Афродита", "Сергей")
        );
        String letter = "А";
        sortAndPrint(list, letter);
    }

    private static void sortAndPrint(List<List<String>> list, String letter) {
        PrintStream ps = new PrintStream(System.out, false, StandardCharsets.UTF_8);
        String string = list.stream()
                .flatMap(List::stream)
                .filter(Objects::nonNull)
                .filter(i -> i.startsWith(letter))
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.joining(", "));
        ps.println(string);
        ps.flush();
        ps.close();;
    }
}
