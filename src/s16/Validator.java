package s16;

@FunctionalInterface
public interface Validator<T> {
    boolean validate(T t);
}
